# Aumentar espaço numa partição LVM

Site de ajuda

*  https://www.rootusers.com/lvm-resize-how-to-increase-an-lvm-partition/


**1 - Verificar as partições e o file system**

`$ lsblk -f`

NAME   FSTYPE FSVER LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINT
sda                                                                             
sdb                                                                             
├─sdb1 vfat   FAT32       5528-FFB7                               590,3M     1% /boot/efi
├─sdb2 xfs                3162b84a-5afe-4d99-8d4b-6e0141db9635    782,6M    23% /boot
└─sdb3 LVM2_m LVM2        9psev5-slPV-ZMaU-gvyA-lLMO-PK4w-wM2JKY                
  ├─fedora_kstudio-root
  │    xfs                545b0a50-b441-4aaf-b6a1-70398850ac99    400,3G     4% /
  └─fedora_kstudio-swap
       swap   1           a5e76512-4b98-4ca0-87b8-11d02b1b9821                  [SWAP]



**2 - Verificar o PATH do Logical Volume**

`$ sudo lvdisplay`

No exemplo abaixo o meu LV Path:

--- Logical volume ---
  **LV Path                /dev/fedora_kstudio/root**
  LV Name                root
  **VG Name                fedora_kstudio**
  LV UUID                QcS2Rn-I9nS-QaCz-TvU0-5aKT-trsy-3udi67
  LV Write Access        read/write
  LV Creation host, time kstudio, 2020-04-30 23:46:40 +0100
  LV Status              available
    open                 1
  LV Size                415,00 GiB
  Current LE             106240
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0

**3 - Usar o comando lvextend para aumentar o espaço associado ao Path**

No meu caso só tinha 15Gbs aumentei para 400Gbs 

`$ lvextend -L+400G /dev/fedora_kstudio/root`



**4 - Aplicar**

No meu caso o file system é xfs

`$ sudo xfs_growfs /dev/fedora_kstudio/root`

Quem tiver ext4

`$ resize2fs /dev/fedora_kstudio/root`


Feito!
